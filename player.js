class Player {
  x = 0;
  y = 0;
  jumpFrame = 0;
  size = 0;
  jumpLength = 450;
  jumpHeight = 0;
  jumpOffset = 0;
  jumpRot = 0;

  constructor(initX, initY) {
    this.x = initX;
    this.y = initY;
    this.size = Math.min(initX, initY) / 20;
    this.jumpHeight = initY / 10;
  }

  jump() {
    if (this.jumpFrame > 50) return;
    this.jumpFrame = this.jumpLength;
  }

  tick(frameDelta) {
    this.jumpFrame = Math.max(0, this.jumpFrame - frameDelta);
    const delta = 1 - cos((this.jumpFrame / this.jumpLength) * 360);
    this.jumpOffset = delta * this.jumpHeight;
    this.jumpRot = (1 - (this.jumpFrame / this.jumpLength)) * 90;
  }

  show(frameDelta) {
    this.tick(frameDelta);
    const offset = this.size / 2;
    push();
    translate(this.x, this.y - offset - this.jumpOffset);
    rotate(this.jumpRot);
    strokeWeight(1);
    stroke(255);
    fill(220);
    square(0, 0, this.size);
    pop();
  }

  collided(enemy) {
    if (this.jumpFrame > 0) return false;
    if (this.x + (this.size/2) < enemy.collideXmin()) return false;
    if (this.x - (this.size/2) > enemy.collideXmax()) return false;
    return true;
  }


}
