var ground = 0;
var player;
var enemies = [];
var running = true;
var points = 0;
var hasJumped = false;

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  ground = windowHeight - windowHeight / 3;
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  ground = windowHeight - windowHeight / 3;
  player = new Player(windowWidth/2, ground);
  rectMode('center');
  angleMode('degrees');
  frameRate(120);
  for (let i = 0; i < 5; i++) {
    enemies.push(new Enemy(ground));
  }
  textFont("mono");
}

function keyPressed() {
  if (!running) {
    points = 0;
    loop();
  } else if (player) {
    hasJumped = true;
    player.jump();
  }
}

var fps = 0;
var latest = Array.from(Array(100)).map(() => 0);
var DEBUG = false;

function draw() {
  background(0, 10, 25);

  if (DEBUG) {
    fps = frameRate();
    latest.shift();
    latest.push(fps);
    noStroke();
    fill(255);
    textSize(10);
    textAlign('left');
    text(`HIGH: ${Math.max(...latest).toFixed(2)} LOW: ${Math.min(...latest).toFixed(2)} AVG: ${latest.reduce((acc, val) => acc + val / 100, 0).toFixed(2)}`, 10, 20);
  }

  stroke(255);
  noFill();
  line(0, ground, windowWidth, ground);

  const delta = running ? deltaTime : 0;
  if (!running) running = true;


  enemies.forEach(e => e.show(delta));

  player.show(delta);

  textSize(30);
  textAlign('center');
  noStroke();
  fill(200);
  if (player.jumpFrame < 50)
    points += delta;
    text(points.toFixed(0), windowWidth/2, ground+50);

  if (enemies.some(e => {
    const collided = player.collided(e);
    if (collided) e.x = windowWidth + (Math.random() * windowWidth);
    return collided;
  })) {
    running = false;
    noLoop();
  }


  if (!running) {
    fill(255, 128, 128);
    text("Press any key to try again", windowWidth/2, ground+150);
    const highScore = localStorage.getItem("highscore");
    let highNum = 0;
    if (highScore) {
      highNum = parseInt(highScore, 10);
      if (isNaN(highNum)) highNum = 0;
    }
    fill(200);
    if (points > highNum) {
      text("New high score!", windowWidth/2, ground+100);
      localStorage.setItem('highscore', points.toFixed(0));
    } else {
      text(`High score: ${highNum}`, windowWidth/2, ground+100);
    }
  }
  else if (!hasJumped) {
    fill(200);
    text("Press any key to jump", windowWidth/2, ground+100);
  }
}
