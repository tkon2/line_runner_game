class Enemy {
  x = 0;
  y = 0;
  height = 50;
  speed = 0.5;
  width = 50;

  constructor(initY) {
    this.x = windowWidth + (Math.random() * windowWidth);
    this.y = initY;
    this.height = Math.min(this.x, initY) / 20;
    this.width = this.height;
  }

  tick(deltaTime) {
    this.x -= deltaTime * this.speed;
  }

  show(deltaTime) {
    this.tick(deltaTime);
    stroke(255, 15, 15);
    fill(128, 15, 15);
    rect(this.x,this.y - this.height / 2,this.width,this.height);
    if (this.x < -100) this.x = windowWidth + (Math.random() * windowWidth);
  }

  collideXmin() {
   return this.x - this.width/2;
  }
  collideXmax() {

   return this.x + this.width/2;
  }
}
